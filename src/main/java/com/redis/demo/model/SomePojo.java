package com.redis.demo.model;

import java.io.Serializable;

public class SomePojo implements Serializable {

    private String valueA;

    private String valueB;

    public SomePojo() {}

    public SomePojo(String valueA, String valueB) {
        this.valueA = valueA;
        this.valueB = valueB;
    }

    public String getValueA() {
        return valueA;
    }

    public String getValueB() {
        return valueB;
    }

    @Override
    public String toString() {
        return "SomePojo{" +
                "valueA='" + valueA + '\'' +
                ", valueB='" + valueB + '\'' +
                '}';
    }
}
