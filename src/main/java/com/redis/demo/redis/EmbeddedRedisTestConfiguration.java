package com.redis.demo.redis;


import com.redis.demo.model.SomePojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.UUID;


@TestConfiguration
public class EmbeddedRedisTestConfiguration {

    private final redis.embedded.RedisServer redisServer;


    @Autowired
    RedisService redisService;

    public EmbeddedRedisTestConfiguration(@Value("${spring.redis.port}") final int redisPort) throws IOException {
        this.redisServer = new redis.embedded.RedisServer(redisPort);
    }

    @PostConstruct
    public void startRedis() {
        this.redisServer.start();

        SomePojo pojo = new SomePojo("123", "345");

        // stores data in cache uses PUT operation
        String uuid = UUID.randomUUID().toString();
        this.redisService.putValueData(uuid, pojo);

        // get value just stored using the same uuid
        // todo add logging
        System.out.println("value returned {}  with id of " + uuid + " -> " + this.redisService.getValueData(uuid));
    }




    @PreDestroy
    public void stopRedis() {
        this.redisServer.stop();
    }
}