package com.redis.demo.redis;

import com.redis.demo.model.SomePojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;

@Service
public class RedisService {

    @Autowired
    RedisUtil<SomePojo> redisUtil;

    public void putValueData(String key, SomePojo pojo) {
        this.redisUtil.putValue(key, pojo);

        // System.out.println("value retrieved form cache ->" + (SomePojo) this.getSomeData("some_key"));

        // SomePojo p = (SomePojo) this.getSomeData("some_key");

        // System.out.println("## dats ->" + p.getValueA());
    }

    public SomePojo getValueData(String key) {
        return this.redisUtil.getValue(key);
    }


    // provides method for serialization of object
    // not explicitly needed as redisTemplate provides serialization.
    public byte[] serializePojo(SomePojo pojo) {
        byte [] data = new byte[0];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(pojo);
            oos.flush();
            data = bos.toByteArray();
            System.out.println("byte array ->" + Arrays.toString(data));
        } catch(Exception e) {
            e.printStackTrace();
        }
        return data;
    }


}
